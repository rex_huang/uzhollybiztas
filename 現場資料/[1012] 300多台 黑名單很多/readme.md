# 1012

## 0925重上電24小時過後

- 早上組網135台
- 下午組網27x台, 還有100台還沒組上
- 原本去的理由是0918日凍結7%, 0925五點時看到0918已經70%了, 對這區塊重啟集中器是有幫助的
- 下午拉回來的拓譜在 [圖](09261730.jpg) [通訊質量](1012_0925_reboot_24h_CommunicationQualityMeterList.xlsx) 可以看到一跳占多數


## 外接上去的表

- 一開始因為沒白名單組不進去
- 鄰居很多
- 組網進去之後也都是weaklink, 有ping成功一台, 但更多是ping沒回

```
[000](000/255 min): 0x038D  00/1C   0/0(1)     0/0   FFFFFF/800003  0/0    0/ 0C
[001](000/253 min): 0x027B  00/25   0/0(2)     0/0   FFFFFF/00003E  0/0    0/ 0C
[002](000/255 min): 0x060C  00/3D   0/0(2)     0/0   FFFFFF/8001FE  0/0    0/ 0C
[003](000/255 min): 0x0000  00/66   0/0(3)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[004](000/254 min): 0x04F3  00/37   0/0(3)     0/0   FFFFFF/0000FC  0/0    0/ 0C
[005](000/253 min): 0x0190  00/1C   0/0(2)     0/0   FFFFFF/000070  0/0    0/ 0C
[006](000/253 min): 0x0271  00/1D   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[007](000/254 min): 0x03A0  00/36   0/0(2)     0/0   FFFFFF/00003F  0/0    0/ 0C
[008](000/253 min): 0x0002  00/44   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[009](000/253 min): 0x044B  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[010](000/253 min): 0x0663  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[011](000/253 min): 0x02CC  00/24   0/0(2)     0/0   FFFFFF/000078  0/0    0/ 0C
[012](000/254 min): 0x06CC  00/1C   0/0(2)     0/0   FFFFFF/000038  0/0    0/ 0C
[013](000/254 min): 0x0008  00/5E   0/0(2)     0/0   FFFFFF/80007F  0/0    0/ 0C
[014](000/253 min): 0x06AD  00/1E   0/0(3)     0/0   FFFFFF/00007C  0/0    0/ 0C
[015](000/255 min): 0x0380  00/1D   0/0(1)     0/0   FFFFFF/00001E  0/0    0/ 0C
[016](000/255 min): 0x03C8  00/4F   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[017](000/255 min): 0x03BB  00/21   0/0(1)     0/0   FFFFFF/00001E  0/0    0/ 0C
[018](000/253 min): 0x0197  00/1C   0/0(2)     0/0   FFFFFF/000038  0/0    0/ 0C
[019](000/253 min): 0x0377  00/2A   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[020](000/255 min): 0x0371  00/3B   0/0(3)     0/0   FFFFFF/80007F  0/0    0/ 0C
[021](000/254 min): 0x072F  00/1D   0/0(2)     0/0   FFFFFF/00007C  0/0    0/ 0C
[022](000/254 min): 0x0154  00/43   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[023](000/254 min): 0x0386  00/1E   0/0(1)     0/0   FFFFFF/000018  0/0    0/ 0C
[024](000/254 min): 0x0420  00/1C   0/0(3)     0/0   FFFFFF/00003F  0/0    0/ 0C
[025](000/254 min): 0x01F8  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[026](000/254 min): 0x008C  00/1C   0/0(2)     0/0   FFFFFF/80007C  0/0    0/ 0C
[027](000/255 min): 0x06CA  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[028](000/255 min): 0x064F  00/29   0/0(2)     0/0   FFFFFF/80007C  0/0    0/ 0C
[029](000/255 min): 0x0449  00/34   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[030](000/255 min): 0x0206  00/1C   0/0(1)     0/0   FFFFFF/000030  0/0    0/ 0C
[031](000/255 min): 0x02EF  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[064](002/255 min): 0xFFFF  00/00   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C

Neighbor Table : [000]-[64] TX Use Neighbor table
      TMR/Neighgor   Dest    LQI  Mod(expect) Scheme    ToneMap    TXRES TXGAINe
[000](000/254 min): 0x038D  00/1D   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[001](000/253 min): 0x027B  00/21   0/0(2)     0/0   FFFFFF/00007E  0/0    0/ 0C
[002](000/252 min): 0x060C  00/30   0/0(3)     0/0   FFFFFF/00007C  0/0    0/ 0C
[003](000/254 min): 0x0000  00/5E   0/0(3)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[004](000/255 min): 0x04F3  00/2A   0/0(3)     0/0   FFFFFF/00007C  0/0    0/ 0C
[005](000/252 min): 0x0190  00/1C   0/0(3)     0/0   FFFFFF/000038  0/0    0/ 0C
[006](000/249 min): 0x0271  00/25   0/0(1)     0/0   FFFFFF/000078  0/0    0/ 0C
[007](000/255 min): 0x03A0  00/2C   0/0(2)     0/0   FFFFFF/80001F  0/0    0/ 0C
[008](000/254 min): 0x0002  00/2D   0/0(2)     0/0   FFFFFF/80003E  0/0    0/ 0C
[009](000/247 min): 0x044B  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[010](000/247 min): 0x0663  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[011](000/250 min): 0x02CC  00/3C   0/0(2)     0/0   FFFFFF/000018  0/0    0/ 0C
[012](000/253 min): 0x06CC  00/1D   0/0(1)     0/0   FFFFFF/00003C  0/0    0/ 0C
[013](000/252 min): 0x0008  00/37   0/0(2)     0/0   FFFFFF/D000FF  0/0    0/ 0C
[014](000/252 min): 0x06AD  00/1C   0/0(2)     0/0   FFFFFF/000066  0/0    0/ 0C
[015](000/254 min): 0x0380  00/1F   0/0(2)     0/0   FFFFFF/00000C  0/0    0/ 0C
[016](000/255 min): 0x03C8  00/1C   0/0(1)     0/0   FFFFFF/800010  0/0    0/ 0C
[017](000/255 min): 0x03BB  00/1C   0/0(1)     0/0   FFFFFF/00001E  0/0    0/ 0C
[018](000/251 min): 0x0197  00/22   0/0(2)     0/0   FFFFFF/00003C  0/0    0/ 0C
[019](000/252 min): 0x0377  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[020](000/255 min): 0x0371  00/4B   0/0(2)     0/0   FFFFFF/C000FF  0/0    0/ 0C
[021](000/254 min): 0x072F  00/1C   0/0(2)     0/0   FFFFFF/8000FC  0/0    0/ 0C
[022](000/253 min): 0x0154  00/1C   0/0(2)     0/0   FFFFFF/00007E  0/0    0/ 0C
[023](000/251 min): 0x0386  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[024](000/254 min): 0x0420  00/23   0/0(2)     0/0   FFFFFF/00007E  0/0    0/ 0C
[025](000/254 min): 0x01F8  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[026](000/252 min): 0x008C  00/4C   0/0(2)     0/0   FFFFFF/00003C  0/0    0/ 0C
[027](000/254 min): 0x06CA  00/35   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[028](000/252 min): 0x064F  00/22   0/0(2)     0/0   FFFFFF/C0007E  0/0    0/ 0C
[029](000/252 min): 0x0449  00/1C   0/0(1)     0/0   FFFFFF/000034  0/0    0/ 0C
[030](000/248 min): 0x0206  00/1C   0/0(1)     0/0   FFFFFF/000030  0/0    0/ 0C
[031](000/253 min): 0x02EF  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[032](000/249 min): 0x01F0  00/1C   0/0(2)     0/0   FFFFFF/000038  0/0    0/ 0C
[033](000/251 min): 0x0360  00/24   0/0(2)     0/0   FFFFFF/00003C  0/0    0/ 0C
[034](000/255 min): 0x0284  00/1C   0/0(1)     0/0   FFFFFF/00003C  0/0    0/ 0C
[035](000/255 min): 0x02A6  00/1C   0/0(2)     0/0   FFFFFF/00001C  0/0    0/ 0C
[036](000/250 min): 0x0228  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[037](000/255 min): 0x036F  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[038](000/250 min): 0x0461  00/1F   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[039](000/254 min): 0x046C  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[040](000/250 min): 0x0379  00/1C   0/0(1)     0/0   FFFFFF/800004  0/0    0/ 0C
[041](000/254 min): 0x0375  00/2F   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[042](000/251 min): 0x038E  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[043](000/251 min): 0x03CD  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[044](000/251 min): 0x03F6  00/1C   0/0(1)     0/0   FFFFFF/000030  0/0    0/ 0C
[045](000/251 min): 0x0211  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[046](000/252 min): 0x015A  00/1C   0/0(1)     0/0   FFFFFF/000014  0/0    0/ 0C
[047](000/253 min): 0x01B5  00/1E   0/0(1)     0/0   FFFFFF/00001E  0/0    0/ 0C
[048](000/252 min): 0x03F4  00/2F   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[049](000/252 min): 0x04C2  00/1C   0/0(2)     0/0   FFFFFF/000078  0/0    0/ 0C
[050](000/252 min): 0x0239  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[051](000/252 min): 0x0479  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[052](000/255 min): 0x007E  00/1C   0/0(1)     0/0   FFFFFF/80001C  0/0    0/ 0C
[053](000/253 min): 0x013E  00/1C   0/0(1)     0/0   FFFFFF/000030  0/0    0/ 0C
[054](000/253 min): 0x0760  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[055](000/255 min): 0x017B  00/1C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[056](000/255 min): 0x050B  00/1C   0/0(3)     0/0   FFFFFF/00007C  0/0    0/ 0C
[057](000/254 min): 0x0772  00/1C   0/0(3)     0/0   FFFFFF/000038  0/0    0/ 0C
[058](000/255 min): 0x0392  00/2C   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C
[059](000/255 min): 0x047E  00/1C   0/0(2)     0/0   FFFFFF/000038  0/0    0/ 0C
[064](002/255 min): 0xFFFF  00/00   0/0(0)     0/0   FFFFFF/FFFFFF  0/0    0/ 0C

```
看到beacon req
```
Receive beacon_request
    ->form: 048906FFFE020000

Receive beacon_request
    ->form: 860407FFFE020000

Receive beacon_request
    ->form: 699206FFFE020000

Receive beacon_request
    ->form: 699206FFFE020000
```

bbp

```
TDP_FIR3_SCALE
    fir3_scale               : 0x00000086(134)
    fir3_scale_max           : 0x00000086(134)
    fir3_scale_min           : 0x0000000A(10)
    fir3_tx_success_cnt      : 0x00000035(53)
    fir3_add_3db_thr         : 0x0000000F(15)
    urgent_in_cnt            : 0x00000000(0)
    ot_event_cnt             : 0x00000000(0)
    start_specific_tx_scale  : 0x00000086(134)
    last_abnormal_tx_scale   : 0x00000086(134)
PHY_RX_MIB_CNT
    preamb_det                  : 3845
    syncm_det_success           : 3167 (Fail 678)
    fch_crc_correct             : 2510 (fch cnt error)
    ack_nack_fch_correct        : 382
    ACK/NACK/Unmatch_correct    : 29 / 0 / 1
    data_fch_correct            : 2125
    data_correct                : 1798 (Fail 327)
    apdt_basic_time_block(btb)  : 1251
    apdt_preamb_det_scale       : 20 (Max 20, Min 0)
    apdt_preamb_noise_thr       : 30 (Max 30, Min0)
    apdt_latched_fa_cnt_per_btb : 1 (AVG 0.54)
    apdt_scale_event            : 0(Up), 0(Dn)
    apdt_huge_fa_in_rx_event    : 0
    syncm_location              :    0     1     2     3     4     5
                                   272  2678    85    28    25    79 (counter)
    FCH Error                   : 2, (format 9C B2 43 12  CD 3B DE E5)
PHY_TX_MIB_CNT               Data     ACK    NACK
    mac_send_cnt         :     66      27      12
    mac_abort_cnt        :      0       0       0
    mac_drop_cnt         :      0       0
    retransmit_cnt       : 8
    data_param_err_cnt   : 0

IDLE_GAIN_STATISTIC(curr 36)
    analog_gain         : 18
    digital_gain        : 18
    igt_mib_process_cnt : 7692
    igt_mib_drop_cnt    : 55
    igt ( >=90dB)      : 0
    igt (   84dB)      : 0
    igt (   78dB)      : 0
    igt (   72dB)      : 0
    igt (   66dB)      : 0
    igt (   60dB)      : 4
    igt (   54dB)      : 4
    igt (   48dB)      : 4
    igt (   42dB)      : 142
    igt (   36dB)      : 7426
    igt (   30dB)      : 57
IGT_FORCE
    freeze              : 0
    analog_gain         : 18
    digital_gain        : 18
    cic1_pwr_max        : 34903 (expect 11400 ~ 22800)
    cic1_pwr_tgt        : 7600
    rx_filt_out_pwr_max : 33413 (expect 5700 ~ 11400)
    rx_filt_out_pwr_tgt : 3800

LQI[28] = 255
LQI[29] = 38
LQI[30] = 26
LQI[31] = 28
LQI[32] = 10
LQI[33] = 16
LQI[34] = 25
LQI[35] = 22
LQI[36] = 13
LQI[37] = 11
LQI[38] = 15
LQI[39] = 14
LQI[40] = 11
LQI[41] = 19
LQI[42] = 19
LQI[43] = 16
LQI[44] = 20
LQI[45] = 16
LQI[46] = 15
LQI[47] = 14
LQI[48] = 15
LQI[49] = 9
LQI[50] = 7
LQI[51] = 5
LQI[52] = 9
LQI[53] = 30
LQI[54] = 16
LQI[55] = 21
LQI[56] = 15
LQI[57] = 10
LQI[58] = 14
LQI[59] = 9
LQI[60] = 11
LQI[61] = 12
LQI[62] = 9
LQI[63] = 5
LQI[64] = 8
LQI[65] = 8
LQI[66] = 8
LQI[67] = 9
LQI[68] = 9
LQI[69] = 7
LQI[70] = 4
LQI[72] = 4
LQI[73] = 4
LQI[74] = 2
LQI[75] = 3
LQI[76] = 1
LQI[77] = 4
LQI[78] = 9
LQI[79] = 1
LQI[80] = 5
LQI[81] = 5
LQI[82] = 4
LQI[83] = 3
LQI[84] = 5
LQI[85] = 5
LQI[86] = 2
LQI[87] = 5
LQI[90] = 1
LQI[91] = 4
LQI[92] = 7
LQI[93] = 4
LQI[94] = 7
LQI[95] = 9
LQI[96] = 9
LQI[98] = 7
LQI[99] = 11
LQI[100] = 4
LQI[102] = 6
LQI[103] = 3
LQI[104] = 1
LQI[106] = 2
AVG LQI = 47.8 (SNR 1.205)
STD LQI = 20.9
SNR[-3] = 255
SNR[-2] = 80
SNR[-1] = 71
SNR[0] = 59
SNR[1] = 71
SNR[2] = 53
SNR[3] = 51
SNR[4] = 62
SNR[5] = 46
SNR[6] = 30
SNR[7] = 33
SNR[8] = 12
SNR[9] = 10
SNR[10] = 20
SNR[11] = 17
SNR[12] = 7
SNR[13] = 16
SNR[14] = 25
SNR[15] = 22
SNR[16] = 10
SNR[17] = 2
SNR_MIB    Preamble  Data
current  :        0    10
   0 dB  :     1578     0
   1 dB  :      150     0
   2 dB  :      128     0
   3 dB  :      128     0
   4 dB  :      156     0
   5 dB  :      114     0
   6 dB  :       96     0
   7 dB  :      129    25
   8 dB  :       85   154
   9 dB  :       64   196
  10 dB  :       65   420
  11 dB  :       26   287
  12 dB  :       27   201
  13 dB  :       46   180
  14 dB  :       26   195
  15 dB  :       20   184
  16 dB  :       58   125
  17 dB  :      167    97
  18 dB  :      169    80
  19 dB  :       72    30
  20 dB  :       34     8
  21 dB  :        3   471

vt# Receive Beacon
Beacon state 2
```

ex meter route
是可以通訊的

```
Device     : LBD (FFD) Pan 0x68F8  Joined
Short Addr : 0x078C (RC_COORD 0x0129)
Extended Addr : 0x8311 39FF FE02 0000

Routing Table : [000]-[511]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[000](65534m17s):  0x0000   0x03A0     0x0129        2             2
[001](65532m27s):  0x0019   0x02CC     0x0097        3             3
[002](65534m16s):  0x02A3   0x027B     0x0043        2             2
[003](65532m33s):  0x02CC   0x02CC     0x002A        1             1
[004](65534m56s):  0x027B   0x027B     0x002A        1             1
[005](65534m55s):  0x03A0   0x03A0     0x002A        1             1
[006](65534m55s):  0x03E1   0x03A0     0x004B        2             1
[007](65534m20s):  0x064F   0x064F     0x002A        1             1
[008](65534m56s):  0x01C2   0x027B     0x0047        2             1

BlackList Table : [000]-[127]
                    Dest
[000](00007m42s):  0x0242
[001](00008m19s):  0x0005

BlackList History : [00]-[127]
         Dest
[00] :  0x0242
[01] :  0x0005

RREQ History : [00]-[31]
         Dest    Ori     Seq
[00]     0000    0019     6663
[01]     026f    0000     35608
[02]     0000    02a3     4558
[03]     0000    026f     1028
[04]     0000    027b     2621
[05]     01c2    0000     35607
[06]     02a3    0000     35609
[07]     01c2    04c3     1579

vt# pinge Add Neighbor Table Fail(BlackList 0x0005)
Drop RREQ(BlackList 0x0005)
Add Neighbor Table Fail(BlackList 0x0005)
>> holley_send_heartbeat(20)

      68 0e 0e 68 5a 99 99 06 82 05 55 30 00 aa aa 00
      01 00 77 16

1c2


Ping Test : size 7, loops 1
.
Total Send Tx/Rx : 1/1, AvgTime 529 msec
PER : 0.0 %

vt# pinge 1c2
Send ToneMap Response(0000), Mod D-3 0xFF7FFF



Ping Test : size 7, loops 1
.
Total Send Tx/Rx : 1/1, AvgTime 216 msec
PER : 0.0 %

vt# pinge 1c2


Ping Test : size 7, loops 1
.
Total Send Tx/Rx : 1/1, AvgTime 152 msec
PER : 0.0 %

vt# pinge 1c2


Ping Test : size 7, loops 1
Add D:0000 O:01C2  seq:2159 to RREQ History[8]
.
Total Send Tx/Rx : 1/1, AvgTime 829 msec
PER : 0.0 %

vt# Wait RREP(0x03e1) Timeout
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry success(1), Update Neighor table 0x027B(Robust)

Send ToneMap Response(02CC), Mod D-2 0x00007C
route[ATx Packet Give Up (TxCMP, nack_or_ackto = 3)
vtmac_pkt_do_fragmentation() tmr force = 1
Robust retry success(0), Update Neighor table 0x02CC(Robust)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry success(1), Update Neighor table 0x027B(Robust)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
Set Neighbor Table[4] : 0x027B tmr valid time to zero
Send RREQ, Dest 0x027b, Seq 5, Flag 8
```

dcu ori route

```
vt# route
Route discovery requset usage:
route a [short addr 0x??]
Routing Table : [000]-[099]
                    Dest   Next_Hop  Route_Cost  Hop_Count  Weak_Link_Count
[001](65481m52s):  0x06AD   0x0008     0x0153        3             3
[002](65306m08s):  0x032A   0x0008     0x0340        4             4
[003](65146m10s):  0x0403   0x0371     0x0136        3             1
[004](65314m57s):  0x068D   0x0371     0x017E        5             2
[005](65505m29s):  0x01F9   0x0008     0x0198        5             4
[006](65157m51s):  0x03C4   0x0371     0x025E        4             3
[007](65503m41s):  0x045C   0x03F4     0x023A        4             2
[008](65440m23s):  0x017C   0x0372     0x02DC        7             5
[009](63615m32s):  0x03D3   0x0371     0x05CD        7             8
[010](65427m20s):  0x040C   0x040C     0x00FF        1             1
[011](64375m06s):  0x057A   0x0008     0x0233        9             4
[012](65386m59s):  0x0207   0x0371     0x021B        3             2
[013](65332m04s):  0x0373   0x0008     0x0236        3             2
[014](65476m00s):  0x01B1   0x0372     0x0355        5             3
[015](65353m41s):  0x007E   0x039B     0x0241        3             3
[016](65246m53s):  0x068F   0x0008     0x0228        3             3
[017](65504m17s):  0x00EC   0x0002     0x0228        3             3
[018](65197m36s):  0x042C   0x039B     0x0535       10             7
[019](65220m44s):  0x04BD   0x0371     0x04C4        7             6
[020](65503m09s):  0x0197   0x0002     0x021D        3             2
[021](65253m43s):  0x0477   0x0008     0x0340        4             4
[022](64600m19s):  0x03A2   0x0371     0x0494        5             7
[023](65374m24s):  0x073F   0x0371     0x0473        7             4
[024](65093m09s):  0x0468   0x0371     0x0246        4             3
[025](63523m59s):  0x0377   0x0371     0x06C5        7             9
[026](65507m36s):  0x0360   0x0371     0x0217        3             2
[027](65530m10s):  0x0381   0x03B0     0x0129        2             2
[028](65475m49s):  0x0423   0x0371     0x023B        4             2
[029](65124m14s):  0x01F0   0x0371     0x044E        5             4
[030](65475m04s):  0x012E   0x012E     0x00FF        1             1
[031](62877m45s):  0x03A6   0x0371     0x06D4        7            10
[032](65125m29s):  0x01A3   0x0371     0x0597        7             6
[033](65414m35s):  0x0380   0x0008     0x0129        2             2
[034](64783m53s):  0x00F3   0x0371     0x052F        6             5
[035](64534m59s):  0x03AB   0x0371     0x084E       10            12
[036](65370m08s):  0x0078   0x0371     0x007E        3             3
[037](64527m33s):  0x0416   0x0008     0x0479        6             5
[038](65165m30s):  0x03CF   0x0371     0x0148        3             2
[039](65245m26s):  0x0472   0x0371     0x0241        4             3
[040](65330m34s):  0x03BC   0x0008     0x0144        3             2
[041](65200m38s):  0x03DF   0x0371     0x05D4        8             7
[042](64609m54s):  0x03CB   0x0371     0x0505        7             9
[043](65509m21s):  0x03B3   0x0372     0x011B        2             1
[044](65462m22s):  0x000F   0x0372     0x0354        5             3
[045](65449m02s):  0x0123   0x0372     0x05F4       10             6
[046](65247m06s):  0x0479   0x0371     0x021A        3             2
[047](65377m20s):  0x01E8   0x0371     0x0153        3             3
[048](65424m49s):  0x0310   0x0003     0x0484        6             5
[049](65522m52s):  0x0001   0x0001     0x00FF        1             1
[050](65520m38s):  0x05CC   0x05CC     0x00FF        1             1
[051](65002m35s):  0x029F   0x0008     0x014A        3             2
[052](65438m29s):  0x036F   0x0371     0x0129        2             2
[053](65198m30s):  0x03ED   0x0008     0x0147        3             2
[054](65521m26s):  0x04F3   0x0002     0x01FE        2             2
[055](65022m08s):  0x02B5   0x0008     0x0153        3             3
[056](65258m08s):  0x0285   0x039B     0x054A       10             9
[057](63684m29s):  0x066F   0x0371     0x07EC        9            10
[058](64851m52s):  0x01F2   0x0008     0x043E        5             5
[059](64230m41s):  0x01F4   0x0371     0x07CB        8            10
[060](65077m37s):  0x03BF   0x0371     0x0550        6             5
[061](65417m59s):  0x03E1   0x03E1     0x00FF        1             1
[062](65492m34s):  0x01B5   0x0372     0x0330        4             3
[063](64862m27s):  0x00C8   0x0371     0x0839       12            10
[064](65473m59s):  0x009C   0x009C     0x001D        1             0
[065](65017m33s):  0x0035   0x0371     0x0534        5             5
[066](65274m51s):  0x0237   0x0008     0x0631        6             6
[067](64840m36s):  0x0031   0x0008     0x055F        6             6
[068](65521m04s):  0x05D9   0x0411     0x0152        4             1
[069](65422m30s):  0x041D   0x0008     0x027D        5             3
[070](64850m05s):  0x06D7   0x0008     0x056F        7             5
[071](65203m42s):  0x03EE   0x0371     0x0139        3             1
[072](65368m09s):  0x0393   0x0371     0x007E        3             3
[073](64731m59s):  0x02F0   0x0008     0x0228        3             3
[074](64025m59s):  0x00AD   0x0371     0x05D1        7             8
[075](65378m36s):  0x0765   0x0372     0x037C        6             4
[076](64935m04s):  0x0221   0x0008     0x0161        4             2
[077](65374m56s):  0x04A9   0x039B     0x0348        4             4
[078](65204m21s):  0x03F7   0x039B     0x0532       10             7
[079](65297m54s):  0x0409   0x039B     0x0396        7             4
[080](65419m49s):  0x029D   0x0008     0x0236        4             2
[081](64984m14s):  0x00CA   0x0371     0x0635        6             6
[082](65097m36s):  0x0235   0x0008     0x0238        4             2
[083](65458m19s):  0x03BA   0x0008     0x0129        2             2
[084](65509m09s):  0x060C   0x0008     0x0166        4             2
[085](65438m27s):  0x0194   0x0003     0x0391        6             5
[086](65383m17s):  0x0705   0x039B     0x0348        4             4
[087](65442m04s):  0x0158   0x0372     0x0607       10             7
[088](65343m38s):  0x03B1   0x0372     0x013B        3             1
[089](64386m36s):  0x0045   0x0008     0x031B        9             4
[090](65523m29s):  0x0372   0x0372     0x00FF        1             1
[091](65530m05s):  0x03B0   0x03B0     0x00FF        1             1
[092](64984m51s):  0x01F5   0x0008     0x0B9C       10            12
[093](65089m36s):  0x03E9   0x0008     0x09D1       10            11
[094](65376m12s):  0x02EF   0x0371     0x045D        6             5
[095](65099m47s):  0x0412   0x0371     0x038D        6             5
[096](65523m06s):  0x064F   0x03A0     0x01FE        2             2
[097](65367m11s):  0x038B   0x0371     0x006F        3             2
[098](65512m59s):  0x03BB   0x03BB     0x00FF        1             1
[099](65193m08s):  0x039E   0x0008     0x07AA        8            10
[100](65512m24s):  0x038D   0x038D     0x00FF        1             1
[101](65376m45s):  0x072F   0x0008     0x021A        3             2
[102](62676m17s):  0x0279   0x0371     0x07FD       10            10
[103](65438m54s):  0x015F   0x03A0     0x018D        4             3
[104](65193m57s):  0x0391   0x0008     0x049A        7             5
[105](65471m38s):  0x0420   0x0008     0x021B        3             2
[106](64601m07s):  0x03E6   0x0371     0x0509        7             9
[107](65157m58s):  0x0376   0x0008     0x0142        3             2
[108](65534m13s):  0x0256   0x0005     0x0119        2             1
[109](64696m28s):  0x03C6   0x0371     0x037C        5             4
[110](65030m05s):  0x02FC   0x0371     0x0167        4             2
[111](64956m54s):  0x011A   0x0008     0x0170        4             3
[112](65200m21s):  0x0400   0x0008     0x0147        3             2
[113](64003m00s):  0x0439   0x0371     0x06BC        7            10
[114](64996m39s):  0x01F6   0x0008     0x0160        4             2
[115](65360m58s):  0x00A2   0x0371     0x0147        3             2
[116](64342m29s):  0x02E5   0x0371     0x06DB        8            10
[117](65400m54s):  0x0668   0x0372     0x021F        3             2
[118](65225m37s):  0x049B   0x0371     0x021D        3             2
[119](65138m32s):  0x03DB   0x0371     0x04C0        7             6
[120](65502m26s):  0x0476   0x0003     0x016A        4             3
[121](65523m02s):  0x015A   0x0008     0x021F        3             2
[122](65135m15s):  0x03B4   0x0371     0x0148        3             2
[123](65046m11s):  0x029B   0x0371     0x0342        4             4
[124](65070m18s):  0x043A   0x0371     0x035C        5             4
[125](63920m22s):  0x0396   0x0371     0x06D4        7            10
[126](64947m04s):  0x0244   0x0008     0x017D        5             2
[127](65093m03s):  0x04CE   0x0371     0x05E0        9             7
[128](65472m18s):  0x025D   0x0371     0x0228        3             3
[129](65164m50s):  0x039A   0x0008     0x0243        3             3
[130](65295m59s):  0x003F   0x039B     0x037C        6             4
[131](65360m29s):  0x036E   0x0008     0x0153        3             3
[132](65387m24s):  0x035A   0x0003     0x0591        7             7
[133](65382m10s):  0x0211   0x0008     0x01A7        5             3
[134](65527m04s):  0x03E4   0x03E4     0x00FF        1             1
[135](65162m26s):  0x03A9   0x0371     0x04C4        7             6
[136](65403m15s):  0x0138   0x0008     0x02FF        8             4
[137](65291m52s):  0x03AE   0x0371     0x0520       10             6
[138](64017m18s):  0x03FB   0x0371     0x05DB        7             9
[139](64180m41s):  0x0392   0x0008     0x0234        3             2
[140](64902m10s):  0x0183   0x0371     0x02C6        7             4
[141](65529m57s):  0x0397   0x0397     0x00FF        1             1
[142](65274m50s):  0x012F   0x03F9     0x0198        4             4
[143](65362m00s):  0x0099   0x0008     0x0145        3             2
[144](65111m37s):  0x039C   0x0371     0x035C        5             4
[145](65068m45s):  0x0647   0x0371     0x0342        4             4
[146](64766m06s):  0x0136   0x0371     0x0599        8             6
[147](65331m05s):  0x044E   0x0371     0x021F        3             2
[148](65522m11s):  0x06CC   0x0008     0x0219        3             2
[149](65524m14s):  0x04A4   0x01D9     0x0162        4             1
[150](64861m32s):  0x01BD   0x0371     0x03B1        7             5
[151](65524m37s):  0x0008   0x0008     0x00FF        1             1
[152](65235m34s):  0x02F9   0x0008     0x0340        4             4
[153](65463m05s):  0x02A6   0x0372     0x0335        4             3
[154](65140m55s):  0x03D7   0x0371     0x04A7        6             6
[155](65298m28s):  0x0014   0x0003     0x059A        7             8
[156](65409m01s):  0x0205   0x0371     0x0228        3             3
[157](63273m17s):  0x03A5   0x0371     0x07FE        9            10
[158](65506m49s):  0x02CC   0x0008     0x0153        3             3
[159](65485m31s):  0x027B   0x0008     0x00F3        7             3
[160](65474m58s):  0x0401   0x0401     0x002A        1             1
[161](65388m25s):  0x02A4   0x039B     0x0273        4             4
[162](65154m28s):  0x03D6   0x0371     0x04C6        7             6
[163](65090m34s):  0x0239   0x0371     0x023A        4             2
[164](63975m13s):  0x03AA   0x0371     0x0A37       12            13
[165](64838m40s):  0x0512   0x0371     0x02FD        3             3
[166](65383m14s):  0x05F6   0x0372     0x0342        4             4
[167](65204m16s):  0x03DA   0x0008     0x0142        3             2
[168](65524m19s):  0x039D   0x039D     0x00FF        1             1
[169](65434m44s):  0x03C8   0x03C8     0x002A        1             1
[170](65529m56s):  0x000E   0x00D6     0x011C        2             1
[171](65205m36s):  0x06CA   0x0371     0x049A        6             5
[172](65438m49s):  0x0271   0x03A0     0x017D        5             1
[173](65110m56s):  0x0422   0x0008     0x09A7        9            10
[174](65325m26s):  0x0663   0x03F4     0x01B5        5             4
[175](65524m11s):  0x01D9   0x01D9     0x00FF        1             1
[176](65371m50s):  0x0760   0x039B     0x03B4        8             4
[177](65521m38s):  0x00EF   0x00EF     0x00FF        1             1
[178](65326m57s):  0x0441   0x039B     0x03D6        9             4
[179](65524m42s):  0x03A0   0x03A0     0x00FF        1             1
[180](63900m48s):  0x03FF   0x0371     0x05E7        7            10
[181](65334m07s):  0x02E1   0x039B     0x043B       10             7
[182](64866m50s):  0x02D1   0x0371     0x0841       12            10
[183](65217m46s):  0x067F   0x0371     0x0239        4             2
[184](64684m05s):  0x019C   0x0008     0x0146        3             2
[185](64732m55s):  0x029E   0x0008     0x0240        4             2
[186](65301m47s):  0x020C   0x0003     0x0480        6             5
[187](65520m55s):  0x0411   0x0411     0x00FF        1             1
[188](65521m17s):  0x03F4   0x03F4     0x00FF        1             1
[189](65327m24s):  0x0453   0x039B     0x04F6        9             7
[190](65163m47s):  0x03C0   0x0371     0x0147        3             2
[191](65070m16s):  0x01F1   0x0371     0x023E        4             2
[192](65112m45s):  0x02A2   0x0008     0x09B0        9            11
[193](65521m39s):  0x01A6   0x0008     0x021F        3             2
[194](65297m09s):  0x0358   0x0008     0x0291        5             4
[195](63440m40s):  0x0399   0x0371     0x04E8        6             9
[196](64951m16s):  0x037F   0x0371     0x0047        2             1
[197](65265m13s):  0x0449   0x0008     0x047B        7             5
[198](65206m19s):  0x0379   0x0371     0x0129        2             2
[199](65417m18s):  0x0402   0x0003     0x011C        2             1
[200](65522m27s):  0x0003   0x0003     0x00FF        1             1
[201](65392m41s):  0x0316   0x0008     0x0238        4             2
[202](65524m17s):  0x03EB   0x039D     0x0129        2             2
[203](65425m23s):  0x0378   0x0378     0x00FF        1             1
[204](65067m59s):  0x034E   0x0371     0x0342        4             4
[205](65511m12s):  0x0190   0x0371     0x015C        4             1
[206](63928m16s):  0x0375   0x0371     0x06D4        7            10
[207](65405m09s):  0x04C3   0x039B     0x041B        9             7
[208](65106m00s):  0x03F5   0x0008     0x09B0        9            11
[209](65102m26s):  0x040F   0x0008     0x09BE       10            10
[210](65509m25s):  0x050B   0x0002     0x01FE        2             2
[211](65217m23s):  0x062B   0x0008     0x05A9        8             7
[212](65165m33s):  0x03C2   0x0371     0x0145        3             2
[213](65428m48s):  0x0284   0x0002     0x0171        4             3
[214](65524m32s):  0x03C9   0x0008     0x0129        2             2
[215](65477m01s):  0x00FD   0x0008     0x0285        5             3
[216](63902m04s):  0x03E2   0x0371     0x05D9        7             9
[217](65522m25s):  0x03D2   0x0003     0x0142        3             2
[218](65524m39s):  0x000D   0x03A0     0x0129        2             2
[219](63317m53s):  0x03DC   0x0371     0x05D6        7             9
[220](65209m15s):  0x03BD   0x0371     0x04A7        6             6
[221](65298m26s):  0x03EF   0x039B     0x0515        9             7
[222](65525m19s):  0x0371   0x0371     0x00FF        1             1
[223](64851m45s):  0x042B   0x0371     0x081F       11            10
[224](65529m51s):  0x00D6   0x00D6     0x00FF        1             1
[225](64609m57s):  0x019B   0x0008     0x05E6        9             7
[226](65209m51s):  0x0386   0x0008     0x0245        3             3
[227](65532m09s):  0x03B9   0x03B9     0x00FF        1             1
[228](64187m58s):  0x0492   0x0371     0x06BC        7            10
[229](65162m11s):  0x067D   0x0371     0x04BB        7             5
[230](65293m46s):  0x03E8   0x039B     0x037E        6             4
[231](64868m25s):  0x0130   0x0371     0x0824       11            10
[232](65332m50s):  0x03F1   0x0008     0x0153        3             3
[233](65208m30s):  0x04A8   0x0008     0x0672        7             7
[234](65377m26s):  0x035D   0x0008     0x021F        3             2
[235](65534m02s):  0x0005   0x0005     0x00FF        1             1
[236](64789m15s):  0x0215   0x0371     0x0354        5             3
[237](64868m21s):  0x0128   0x0371     0x04B1        8             5
[238](65313m40s):  0x072C   0x0008     0x016C        4             3
[239](64902m32s):  0x01C5   0x0371     0x05A3        7             7
[240](64539m47s):  0x041C   0x0371     0x06A7       12            11
[241](65534m21s):  0x026F   0x078C     0x0176        4             2
[242](65534m29s):  0x0019   0x078C     0x005E        3             1
[243](65475m07s):  0x040D   0x0372     0x0335        4             3
[244](64015m05s):  0x03CD   0x0371     0x06D4        7            10
[245](65133m39s):  0x018A   0x0371     0x0335        4             3
[246](65534m29s):  0x078C   0x078C     0x002A        1             1
[247](65503m33s):  0x0333   0x0008     0x0164        4             2
[248](65534m28s):  0x02A3   0x078C     0x007E        3             3
[249](65246m04s):  0x017B   0x0371     0x021D        3             2
[251](65295m52s):  0x0404   0x039B     0x0520        9             8
[253](65094m15s):  0x075D   0x0371     0x0247        4             3
[254](65280m38s):  0x0374   0x0371     0x011B        2             1
[255](65139m06s):  0x01DA   0x0008     0x088E        8             9
[257](63970m48s):  0x0013   0x0371     0x06CA        7             9
[258](65102m28s):  0x0406   0x0008     0x088B        8             9
[259](65236m01s):  0x04B9   0x0008     0x0241        4             3
[260](65296m39s):  0x0432   0x039B     0x0512        9             7
[262](65102m10s):  0x0428   0x0008     0x09A7        9            10
[264](65299m44s):  0x0733   0x0371     0x04F8        9             5
[266](65237m15s):  0x0455   0x039B     0x061F       10             9
[268](65240m13s):  0x04D2   0x0371     0x0228        3             3

```


## 重接DCU

- 組網速度還算快, 有好通的點
- 有不少再白名單的

```
vt#
vt#
Receive beacon_request
    ->form: 700607FFFE020000
[S->SB] Send_Keepalive(seq:20,len:6, time:4096)

Receive beacon_request
    ->form: 749006FFFE020000

Receive beacon_request
    ->form: 730607FFFE020000

Receive beacon_request
    ->form: 498606FFFE020000

Receive beacon_request
    ->form: 780607FFFE020000
Send Beacon (RC_COORD 0x0)
[S->SB] Send_Keepalive(seq:20,len:6, time:4096)
[S->SB] Send_Keepalive(seq:20,len:6, time:4096)
[S->SB] Send_Keepalive(seq:20,len:6, time:4096)
add task tcip (priority 4) size 24
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0001)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:831139FFFE020000)
[S->SB] Send_Keepalive(seq:20,len:6, time:4096)

Receive beacon_request
    ->form: 929606FFFE020000

Receive beacon_request
    ->form: 410307FFFE020000
LBS: not in whitelist -> decline!!

Receive beacon_request
    ->form: 730607FFFE020000
[S->SB] Send_Keepalive(seq:20,len:6, time:4096)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0002)
[S->D] BootsTx Packet Give Up (TxCMP, nack_or_ackto = 3)
trap: Send_eap_psk_msg3
Robust retry fail
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry success(1)
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:448606FFFE020000)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry success(0)
Same join again
  300059CE | 27 86 06 FF FE 02 00 00

Receive beacon_request
    ->form: 750607FFFE020000
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
LBS: not in whitelist -> decline!!
[S->SB] Send_Keepalive(seq:20,len:6, time:4096)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
[D->S] Bootstrap: Receive_Msg2
Robust retry fail
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0003)
[S->D] Bootstrap: Send_eap_psk_msg3
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail

Receive beacon_request
    ->form: 749006FFFE020000
Send Beacon (RC_COORD 0x0)
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:278606FFFE020000)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
LBS: not in whitelist -> decline!!
Robust retry fail
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
LBS: not in whitelist -> decline!!
Robust retry fail
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)

Receive beacon_request
    ->form: 690607FFFE020000
Robust retry fail
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry success(0)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)

Receive beacon_request
    ->form: 700607FFFE020000

Receive beacon_request
    ->form: 929606FFFE020000
Robust retry fail

Receive beacon_request
    ->form: 770607FFFE020000
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0004)

Receive beacon_request
    ->form: 879606FFFE020000
[S->D] Bootstrap: Send_eap_psk_msg3

Receive beacon_request
    ->form: 939606FFFE020000
[S->SB] Send_Keepalive(seq:20,len:6, time:4096)
Send Beacon (RC_COORD 0x0)
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
vtmac_pkt_do_fragmentation() tmr force = 1
Robust retry fail
Set Neighbor Table[4] : 0x0002 tmr valid time to zero
cmd delete route
add task rxch (priority 6) size 24
LBS: not in whitelist -> decline!!
dcu delete route success

Send ToneMap Response(0002), Mod D-0 0xFFFFFF

Receive beacon_request
    ->form: 730607FFFE020000
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)

Send ToneMap Response(0001), Mod D-3 0xFFF1FF
Robust retry fail

Receive beacon_request
    ->form: 750607FFFE020000
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!

Receive beacon_request
    ->form: 700607FFFE020000
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
[D->S] Bootstrap: Receive_Msg2
Robust retry fail
Set Neighbor Table[4] : 0x0002 tmr valid time to zero

VTSPI_CMD_PACKET_RECEIVE, error subtype B3
cmd delete route
dcu send fail
dcu delete route success

Send ToneMap Response(0002), Mod D-0 0xFFFFFF
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
vtmac_pkt_do_fragmentation() tmr force = 1
Robust retry fail
Set Neighbor Table[4] : 0x0002 tmr valid time to zero
cmd delete route
dcu delete route success
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Send Beacon (RC_COORD 0x0)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
[D->S] Bootstrap: Receive_Msg2
Robust retry success(1)
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0005)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:939606FFFE020000)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
LBS: not in whitelist -> decline!!
Robust retry fail
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
vtmac_pkt_do_fragmentation() tmr force = 1
Robust retry success(1), Update Neighor table 0x0002(Robust)
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
LBS: not in whitelist -> decline!!
Robust retry fail
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0006)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:579706FFFE020000)

Receive beacon_request
    ->form: 890407FFFE020000
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(0002), Mod D-2 0x80005F

Receive beacon_request
    ->form: 690607FFFE020000
LBS: not in whitelist -> decline!!
Send Beacon (RC_COORD 0x0)
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0007)
[S->D] Bootstrap: Send_eap_psk_msg3
(TG) 1
msg3 insert tx queue fail
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
Set Neighbor Table[4] : 0x0002 tmr valid time to zero
cmd delete route
dcu delete route success
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0008)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0009)
[S->D] Bootstrap: Send_eap_psk_msg3
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
vtmac_pkt_do_fragmentation() tmr force = 1

Receive beacon_request
    ->form: 750607FFFE020000

Send ToneMap Response(0002), Mod D-2 0x80007F
Robust retry success(1), Update Neighor table 0x0002(Robust)
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:488606FFFE020000)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Receive beacon_request
    ->form: 750607FFFE020000
LBS: not in whitelist -> decline!!
C(0001)LBS: not in whitelist -> decline!!

Receive beacon_request
    ->form: 171907FFFE020000
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
Set Neighbor Table[4] : 0x0002 tmr valid time to zero
cmd delete route
dcu delete route success
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x000a)
[S->D] Bootstrap: Send_eap_psk_msg3

Receive beacon_request
    ->form: 700607FFFE020000
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:639706FFFE020000)
LBS: not in whitelist -> decline!!
C(0002)
Send ToneMap Response(0002), Mod D-2 0x00001F
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(0005), Mod D-0 0xFFFFFF
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x000b)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
C(0005)C(0005)C(0005)[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x000c)
[S->D] Bootstrap: Send_eap_psk_msg3
Free the same address 30000480
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:589706FFFE020000)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
vtmac_pkt_do_fragmentation() tmr force = 1
C(0005)Robust retry success(1), Update Neighor table 0x0005(Robust)
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
(C)Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
[D->S] Bootstrap: Receive_Robust retry success(1), Update Neighor table 0x0003()
Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(0003), Mod D-1 0x000006
C(0005)
Send ToneMap Response(0005), Mod D-0 0xFFFFFF
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
C(0005)Robust retry fail
Set Neighbor Table[6] : 0x0005 tmr valid time to zero
cmd delete route
dcu delete route success
C(0005)C(0005)Send Beacon (RC_COORD 0x0)
C(0005)[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(0005), Mod D-0 0xFFFFFF
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
dcu send fail
LBS: not in whi
VTSPI_CMD_PACKET_RECEIVE, error subtype B3
telist -> decline!!
Robust retry fail
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(0003), Mod D-1 0x00001E
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x000d)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
C(0005)[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:320307FFFE020000)
C(0001)[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x000e)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Same join again
  300013EE | 08 99 06 FF FE 02 00 00
Same join again
  30000DEE | 08 99 06 FF FE 02 00 00
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Receive beacon_request
    ->form: 438606FFFE020000
LBS: not in whitelist -> decline!!

Send ToneMap Response(0003), Mod D-2 0x000006
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry success(1), Update Neighor table 0x0003(Robust)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Same join again
  3000102E | 37 03 07 FF FE 02 00 00
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x000f)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:370307FFFE020000)
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(0005), Mod D-0 0xFFFFFF
Same join again
  300013EE | 42 03 07 FF FE 02 00 00
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0010)
[S->D] Bootstrap: Send_eap_psk_msg3
C(0005)C(0005)[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Receive beacon_request
    ->form: 690607FFFE020000

Receive beacon_request
    ->form: 750607FFFE020000
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0011)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:910407FFFE020000)
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:659706FFFE020000)
Send Beacon (RC_COORD 0x0)
LBS: not in whitelist -> decline!!

Receive beacon_request
    ->form: 730607FFFE020000
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!

VCS wait 334 ms(3)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
vtmac_pkt_do_fragmentation() tmr force = 1
C(0005)Robust retry success(0), Update Neighor table 0x0005(Robust)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
C(0005)[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0012)
[S->D] Bootstrap: Send_eap_psk_msg3
Free the same address 300002C0
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Same join again
  3000108E | 78 97 06 FF FE 02 00 00
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
C(0005)C(0005)[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0013)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
C(0005)C(0005)[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0014)
[S->D] Bootstrap: Send_eap_psk_msg3
Free the same address 30000080
[D->S] Bootstrap: Receive_Msg2
  300026AE | 94 04 07 FF FE 02 00 00
C(0001)[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0015)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
(TG) 1
msg1 insert tx queue fail
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
(TG) 2
msg1 insert tx queue fail
Send RREP, Dest Addr 0x0002, Next 0x0003, Seq 0, Flag 0, size 14
(C)[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(0005), Mod D-0 0xFFFFFF
Send RREP, Dest Addr 0x0005, Next 0x0001, Seq 1, Flag 0, size 14
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:789706FFFE020000)
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:940407FFFE020000)
(TG) 1
join_accept insert tx queue fail
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
(TG) 2
msg1 insert tx queue fail
C(0005)C(0005)[D->S] Bootstrap: Receive_Msg2
C(0005)[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0016)
[S->D] Bootstrap: Send_eap_psk_msg3
(TG) 3
msg3 insert tx queue fail
LBS: not in whitelist -> decline!!
(TG) 4

Send ToneMap Response(0001), Mod D-3 0xFF5FFF
(TG) 5
LBS: not in whitelist -> decline!!
(TG) 6
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
Set Neighbor Table[6] : 0x0005 tmr valid time to zero
cmd delete route
dcu delete route success
Send Beacon (RC_COORD 0x0)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry success(0)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(0001), Mod D-3 0xFFFFFF
Receive RERR 0x0500 is unreachable
cmd delete route
dcu delete route success
cmd search route
dcu search route success
Send RREQ, Dest 0x0005, Seq 0, Flag 0
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Receive beacon_request
    ->form: 890407FFFE020000
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Free the same address 30000460
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0017)
[S->D] Bootstrap: Send_eap_psk_msg3
LBS: not in whitelist -> decline!!
Route Discovery Success (Dest 0x0005, Next 0x0005, Hop 1)
vtmsg_nwk_event
C(0005)
Receive beacon_request
    ->form: 919606FFFE020000
C(0005)
Receive beacon_request
    ->form: 498606FFFE020000
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)

Send ToneMap Response(0003), Mod D-1 0x000016

Receive beacon_request
    ->form: 700607FFFE020000
Robust retry success(2), Update Neighor table 0x0003(Robust)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Robust retry fail
Set Neighbor Table[5] : 0x0003 tmr valid time to zero
cmd delete route
dcu delete route success
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0018)
[S->D] Bootstrap: Send_eap_psk_msg3
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1


```


s2e


```
ConfirmPacket [00H-F1]
vtsdk_free 0x1000d3f0

vt# s2e

black list OFF
white list ON
                  EUI64                       | Joined | spi_id/pdc | short addr
===========================================================================
[000]:0x23 0x82 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[001]:0x29 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[002]:0x30 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[003]:0x31 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[004]:0x32 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[005]:0x33 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[006]:0x35 0x15 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[007]:0x33 0x17 0x10 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[008]:0x73 0x85 0x14 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[009]:0x06 0x90 0x14 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[010]:0x08 0x90 0x14 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[011]:0x09 0x90 0x14 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[012]:0x79 0x06 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[013]:0x85 0x06 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[014]:0x89 0x06 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[015]:0x09 0x12 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[016]:0x10 0x12 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[017]:0x12 0x49 0x15 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[018]:0x21 0x17 0x16 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[019]:0x83 0x27 0x18 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[020]:0x77 0x79 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[021]:0x78 0x79 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[022]:0x79 0x79 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[023]:0x81 0x79 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[024]:0x82 0x79 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[025already send accept from spi 0
magpie_already_send_accept req->pdc_diff=0, DB.meter_whitelist_table[i].pdc_dif
vtsdk_notify_pan_joined pdc_diff=1
Valid LBD join CB Meter id:79 02 07 02 00 00
EUI64 addr:PHASE: B
79 02 07 ff fe 02 00 00
join_cb >>
79 02 07 02 00 00
join_node meter id is repeat: 79 02 07 02 00 00
Meter id have been added before 79 02 07 02 00 00
join_node Reg Slvae Total: 0
]:0x46 0x85 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          (    )
[026]:0x78 0x92 0x19 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[027]:0x83 0x11 0x39 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x0001 ()
[028]:0x05 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[029]:0x06 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[030]:0x07 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[031]:0x08 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[032]:0x09 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[033]:0x10 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[034]:0x11 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    check white list from spi 0
illegal LBD join CB Meter id:17 19 07 02 00 00
EUI64 addr:17 19 07 ff fe 02 00 00
illegal_cb ILLEGAL ID: 17 19 07 02 00 00
X                          (         0,          0,          0)
[035]:0x12 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[036]:0x13 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[037]:0x14 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[038]:0x15 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[039]:0x16 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0018 ()
[040]:0x17 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[041]:0x18 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[042]:0x19 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[043]:0x20 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[044]:0x21 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[045]:0x22 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[046]:0x23 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[047]:0x24 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[048]:0x25 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[049]:0x26 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[050]:0x27 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x0003 ()
[051]:0x28 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X  dcu_assign_new_addr req->
assign addr 0x0058 to 80 97 06 ff fe 02 00 00
                        (         0,          0,          0)
[052]:0x41 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[053]:0x42 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          (0
      0,          0,          0)
[054]:0x43 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/02   |  0x0004 ()
[055]:0x44 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0002 ()
[056]:0x45 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[057]:0x46 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[058]:0x47 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[059]:0x48 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x0009 ()
[060]:0x49 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[061]:0x50 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[062]:0x51 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[063]:0x52 0x86 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[064]:0x69 0x87 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[065]:0x70 0x87 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          (0
         0)
[066]:0x74 0x87 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[067]:0x75 0x87 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[068]:0x76 0x87 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[069]:0x77 0x87 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[070]:0x78 0x87 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0029 ()
[071]:0x79 0x87 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[072]:0x99 0x88 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[073]:0x00 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[074]:0x01 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[075]:0x02 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[076]:0x03 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[077]:0x04 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[078]:0x05 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x0043 ()
[079]:0x06 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[080]:0x07 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[081]:0x08 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[082]:0x09 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[083]:0x10 0x89 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[084]:0x19 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[085]:0x 79 02 07 0220 0x90 0x0 00 00, Join6 0xFF 0xFE Upload Cnt: 0x02 0x00 0 1
FnUploax00 |    X  dSlaveMsg [            06H-F1] Upl            oad permit(1)
FnU(         0ploadSlaveMs,          g Source (790,          02 07 02 00 0)
[086]: 00)
_vtsdk0x21 0x90 0_send: UART x06 0xFF 0xpbuf=0x10002FE 0x02 0x00f90, len=25 0x0X
                          (         0,          0,          0)
[087]:0x22 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
0x90 0x06 00[088]:0x23 ) send(25):
           68 19 00 c1xFF 0xFE 0x 00 01 31 ff02 0x00 0x0 00 42 06 010 |    X
      07            02 00 00 00  (         000 00 c0 16,          0
,          0)
[089]:0x24 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[090]:0x25 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[091]:0x26 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[092]:0x27 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[093]:0x28 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[094]:0x29 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[095]:0x33 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/02   |  0x0007 ()
[096]:0x34 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[097]:0x35 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[098]:0x36 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[099]:0x38 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[100]:0x39 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[101]:0x40 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[102]:0x41 0x90 0x>>>>>>
tas06 0xFF 0xFEk_loop: Rx  0x02 0x00 0from UART (x00 |    X  21) 1000beb       0
68 15 00             01 00 01 2(         08 80 25 42 00          0 01 00 ff ff,
      0)
[103]:0ff 00 00 0e x42 0x90 0x16
vtsdk_c06 0xFF 0xFalloc 0x1000E 0x02 0x00d3f0(61)
ta 0x00 |    sk_loop PackX          et Serial Nu           m: 66
FnCon     (     firmPacket [    0,      00H-F1]
    0,       vtsdk_free    0)
:0x43 0x90 03f0
            x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          (     )
[105]:0x44 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[106]:0x45 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[107]:0x54 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[108]:0x67 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[109]:0x68 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[110]:0x69 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[111]:0x70 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[112]:0x71 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[113]:0x72 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[114]:0x73 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[115]:0x74 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[116]:0x75 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[117]:0x76 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[118]:0x77 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[119]:0x78 0x90 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[120]:0x61 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[121]:0x62 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/01   |  0x0008 ()
[122]:0x63 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[123]:0x64 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[124]:0x65 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[125]:0x66 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[126]:0x67 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0033 ()
[127]:0x68 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[128]:0x69 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[129]:0x70 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[130]:0x71 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/01   |  0x004a ()
[131]:0x72 0x92 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[132]:0x82 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x0055 ()
[133]:0x83 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[134]:0x84 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[135]:0x85 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[136]:0x87 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x0017 ()
[137]:0x88 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[138]:0x89 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[139]:0x90 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x002e ()
[140]:0x91 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x0020 ()
[141]:0x92 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x0044 ()
[142]:0x93 0x96 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x0005 ()
[143]:0x54 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[144]:0x55 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/02   |  0x0047 ()
[145]:0x56 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x0045 ()
[146]:0x57 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x0006 ()
[147]:0x58 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x0036 ()
[148]:0x59 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[149]:0x60 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[150]:0x61 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[151]:0x62 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x004e ()
[152]:0x63 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x000a ()
[153]:0x64 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[154]:0x65 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x0011 ()
[155]:0x78 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0015 ()
[156]:0x79 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x0042 ()
[157]:0x80 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x0058 ()
[158]:0x81 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[159]:0x82 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x004c ()
[160]:0x83 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0025 ()
[161]:0x84 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[162]:0x85 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[163]:0x86 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[164]:0x87 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x000b ()
[165]:0x88 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/02   |  0x0050 ()
[166]:0x89 0x97 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x0012 ()
[167]:0x01 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[168]:0x02 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[169]:0x03 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[170]:0x04 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[171]:0x05 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[172]:0x06 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[173]:0x07 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0049 ()
[174]:0x08 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X               dcu_assign_n
assign addr 0x0059 to 23 07 07 ff fe 02 00 00
           (         0,          0,          0)
[175]:0x09 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/01   |  0x0034 ()
[176]:0x10 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[177]:0x11 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[178]:0x12 0x99 0x06 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[179]:0x47 0x00 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[180]:0x48 0x00 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[181]:0x49 0x00 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[182]:0x50 0x00 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[183]:0x71 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[184]:0x72 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[185]:0x73 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[186]:0x74 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x0035 ()
[187]:0x75 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0019 ()
[188]:0x76 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[189]:0x77 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[190]:0x78 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x0048 ()
[191]:0x79 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x0056 ()
[192]:0x80 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[193]:0x81 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/01   |  0x002d ()
[194]:0x82 0x02 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[195]:0x31 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/01   |  0x0057 ()
[196]:0x32 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x000d ()
[197]:0x33 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x004d ()
[198]:0x34 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[199]:0x35 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x002b ()
[200]:0x36 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[201]:0x37 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x000f ()
[202]:0x38 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x001d ()
[203]:0x39 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/01   |  0x0028 ()
[204]:0x40 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[205]:0x41 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[206]:0x42 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x001f ()
[207]:0x92 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[208]:0x93 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[209]:0x94 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[210]:0x95 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[211]:0x96 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[212]:0x97 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[213]:0x98 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[214]:0x99 0x03 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0040 ()
[215]:0x00 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[216]:0x01 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[217]:0x02 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x003d ()
[218]:0x03 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[219]:0x76 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[220]:0x77 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[221]:0x78 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[222]:0x79 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[223]:0x80 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[224]:0x81 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[225]:0x82 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[226]:0x83 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[227]:0x84 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[228]:0x85 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[229]:0x86 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[230]:0x87 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[231]:0x88 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0021 ()
[232]:0x89 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x002a ()
[233]:0x90 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[234]:0x91 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0010 ()
[235]:0x92 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[236]:0x93 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/01   |  0x003f ()
[237]:0x94 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0014 ()
[238]:0x95 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x001a ()
[239]:0x96 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x003e ()
[240]:0x97 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0024 ()
[241]:0x98 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[242]:0x99 0x04 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x003b ()
[243]:0x12 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[244]:0x13 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[245]:0x14 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[246]:0x15 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[247]:0x16 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/02   |  0x0026 ()
[248]:0x17 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[249]:0x18 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[250]:0x19 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[251]:0x20 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[252]:0x21 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[253]:0x22 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[254]:0x23 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[255]:0x60 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[256]:0x61 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[257]:0x62 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[258]:0x63 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[259]:0x64 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[260]:0x65 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[261]:0x66 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[262]:0x67 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[263]:0x68 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[264]:0x69 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[265]:0x70 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[266]:0x71 0x05 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[267]:0x08 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[268]:0x09 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[269]:0x10 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[270]:0x11 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[271]:0x12 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[272]:0x13 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[273]:0x14 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[274]:0x15 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[275]:0x16 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[276]:0x17 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[277]:0x18 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[278]:0x19 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[279]:0x56 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[280]:0x57 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/02   |  0x004b ()
[281]:0x58 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0054 ()
[282]:0x59 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[283]:0x60 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x001c ()
[284]:0x61 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x003a ()
[285]:0x62 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x0051 ()
[286]:0x63 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[287]:0x64 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[288]:0x65 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x004f ()
[289]:0x66 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[290]:0x68 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x0052 ()
[291]:0x69 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[292]:0x71 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[293]:0x72 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[294]:0x73 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x001b ()
[295]:0x74 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[296]:0x75 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[297]:0x76 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x0032 ()
[298]:0x77 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[299]:0x78 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/01   |  0x0031 ()
[300]:0x79 0x06 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[301]:0x16 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0037 ()
[302]:0x17 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/02   |  0x0046 ()
[303]:0x18 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x0053 ()
[304]:0x19 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0023 ()
[305]:0x20 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[306]:0x21 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[307]:0x22 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x0038 ()
[308]:0x23 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x0059 ()
[309]:0x24 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/02   |  0x002f ()
[310]:0x25 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    O   |    00/00   |  0x0039 ()
[311]:0x26 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[312]:0x27 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/02   |  0x003c ()
[313]:0x53 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/01   |  0x0041 ()
[314]:0x54 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[315]:0x55 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[316]:0x56 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[317]:0x57 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[318]:0x58 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[319]:0x59 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[320]:0x60 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[321]:0x61 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[322]:0x62 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[323]:0x63 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[324]:0x64 0x07 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[325]:0x16 0x08 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[326]:0x17 0x08 0x07 0xFF 0xFE 0x02 0x00 0x00 |   AD   |    00/00   |  0x0030 ()
[327]:0x18 0x08 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[328]:0x19 0x08 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[329]:0x20 0x08 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[330]:0x21 0x08 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
[331]:0x22 0x08 0x07 0xFF 0xFE 0x02 0x00 0x00 |    X                          ()
Total 332 (Msg1 251, Msg3 27, Accept 54, ReJoin 0)

show_packet_meter_by_alive
--------------------------------------------------------------------------------
|   idx  |F|R|J|    meter_id       | Phase | SNR |ALIVE| HARBT | Keep Time | Ke|
--------------------------------------------------------------------------------
|   00   |1|0|0| 05 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   01   |1|0|0| 06 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   02   |1|0|0| 07 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   03   |1|0|0| 08 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   04   |1|0|0| 09 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   05   |1|0|0| 10 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   06   |1|0|0| 11 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   07   |1|0|0| 12 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   08   |1|0|0| 13 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   09   |1|0|0| 14 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   10   |1|0|0| 15 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   11   |1|0|1| 16 86 06 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  0|
|   12   |1|0|0| 17 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   13   |1|0|0| 18 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   14   |1|0|0| 19 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   15   |1|0|0| 20 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   16   |1|0|0| 21 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   17   |1|0|0| 22 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   18   |1|0|0| 23 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   19   |1|0|0| 24 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   20   |1|0|0| 25 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   21   |1|0|0| 26 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   22   |1|0|1| 27 86 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  0|
|   23   |1|0|0| 28 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   24   |1|0|0| 41 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   25   |1|0|0| 42 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   26   |1|0|0| 43 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   27   |1|0|1| 44 86 06 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  0|
|   28   |1|0|0| 45 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   29   |1|0|0| 46 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   30   |1|0|0| 47 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   31   |1|0|1| 48 86 06 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  0|
|   32   |1|0|0| 49 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   33   |1|0|0| 50 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   34   |1|0|0| 51 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   35   |1|0|0| 52 86 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   36   |0|0|1| 83 11 39 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  0|
|   37   |1|0|0| 69 87 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   38   |1|0|0| 70 87 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   39   |1|0|0| 74 87 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   40   |1|0|0| 75 87 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   41   |1|0|0| 76 87 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   42   |1|0|0| 77 87 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   43   |1|0|1| 78 87 06 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  0|
|   44   |1|0|0| 79 87 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   45   |1|0|0| 99 88 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   46   |1|0|0| 00 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   47   |1|0|0| 01 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   48   |1|0|0| 02 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   49   |1|0|0| 03 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   50   |1|0|0| 04 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   51   |1|0|0| 05 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   52   |1|0|0| 06 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   53   |1|0|0| 07 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   54   |1|0|0| 08 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   55   |1|0|0| 09 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   56   |1|0|0| 10 89 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   57   |1|0|0| 19 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   58   |1|0|0| 20 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   59   |1|0|0| 21 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   60   |1|0|0| 22 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   61   |1|0|0| 23 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   62   |1|0|0| 24 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   63   |1|0|0| 25 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   64   |1|0|0| 26 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   65   |1|0|0| 27 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   66   |1|0|0| 28 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   67   |1|0|0| 29 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   68   |1|0|0| 33 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   69   |1|0|0| 34 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   70   |1|0|0| 35 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   71   |1|0|0| 36 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   72   |1|0|0| 38 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   73   |1|0|0| 39 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   74   |1|0|0| 40 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   75   |1|0|0| 41 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   76   |1|0|0| 42 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   77   |1|0|0| 43 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   78   |1|0|0| 44 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   79   |1|0|0| 45 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   80   |1|0|0| 54 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   81   |1|0|0| 67 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   82   |1|0|0| 68 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   83   |1|0|0| 69 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   84   |1|0|0| 70 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   85   |1|0|0| 71 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   86   |1|0|0| 72 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   87   |1|0|0| 73 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   88   |1|0|0| 74 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   89   |1|0|0| 75 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   90   |1|0|0| 76 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   91   |1|0|0| 77 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   92   |1|0|0| 78 90 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   93   |1|0|0| 61 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   94   |1|0|0| 62 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   95   |1|0|0| 63 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   96   |1|0|0| 64 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   97   |1|0|0| 65 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   98   |1|0|0| 66 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  0|
|   99   |1|0|1| 67 92 06 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  0|
|   100   |1|0|0| 68 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   101   |1|0|0| 69 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   102   |1|0|0| 70 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   103   |1|0|0| 71 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   104   |1|0|0| 72 92 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   105   |1|0|1| 82 96 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   106   |1|0|0| 83 96 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   107   |1|0|0| 84 96 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   108   |1|0|1| 85 96 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   109   |1|0|1| 87 96 06 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   110   |1|0|0| 88 96 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   111   |1|0|0| 89 96 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   112   |1|0|1| 90 96 06 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   113   |1|0|1| 91 96 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   114   |1|0|1| 92 96 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   115   |1|0|1| 93 96 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   116   |1|0|0| 54 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   117   |1|0|1| 55 97 06 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   118   |1|0|1| 56 97 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   119   |1|0|1| 57 97 06 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   120   |1|0|1| 58 97 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   121   |1|0|0| 59 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   122   |1|0|0| 60 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   123   |1|0|0| 61 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   124   |1|0|0| 62 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   125   |1|0|1| 63 97 06 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   126   |1|0|0| 64 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   127   |1|0|1| 65 97 06 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   128   |1|0|1| 78 97 06 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   129   |1|0|1| 79 97 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   130   |1|0|1| 80 97 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   131   |1|0|0| 81 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   132   |1|0|1| 82 97 06 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   133   |1|0|1| 83 97 06 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   134   |1|0|0| 84 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   135   |1|0|0| 85 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   136   |1|0|0| 86 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   137   |1|0|0| 87 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   138   |1|0|0| 88 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   139   |1|0|0| 89 97 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   140   |1|0|0| 01 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   141   |1|0|0| 02 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   142   |1|0|0| 03 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   143   |1|0|0| 04 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   144   |1|0|0| 05 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   145   |1|0|0| 06 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   146   |1|0|1| 07 99 06 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   147   |1|0|0| 08 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   148   |1|0|0| 09 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   149   |1|0|0| 10 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   150   |1|0|0| 11 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   151   |1|0|0| 12 99 06 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   152   |1|0|0| 47 00 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   153   |1|0|0| 48 00 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   154   |1|0|0| 49 00 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   155   |1|0|0| 50 00 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   156   |1|0|0| 71 02 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   157   |1|0|0| 72 02 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   158   |1|0|1| 73 02 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   159   |1|0|1| 74 02 07 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   160   |1|0|1| 75 02 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   161   |1|0|0| 76 02 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   162   |1|0|0| 77 02 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   163   |1|0|0| 78 02 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   164   |1|0|1| 79 02 07 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   165   |1|0|0| 80 02 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   166   |1|0|0| 81 02 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   167   |1|0|0| 82 02 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   168   |1|0|0| 31 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   169   |1|0|1| 32 03 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   170   |1|0|1| 33 03 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   171   |1|0|0| 34 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   172   |1|0|1| 35 03 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   173   |1|0|0| 36 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   174   |1|0|1| 37 03 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   175   |1|0|1| 38 03 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   176   |1|0|0| 39 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   177   |1|0|0| 40 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   178   |1|0|1| 41 03 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   179   |1|0|0| 42 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   180   |1|0|0| 92 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   181   |1|0|0| 93 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   182   |1|0|0| 94 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   183   |1|0|0| 95 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   184   |1|0|0| 96 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   185   |1|0|0| 97 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   186   |1|0|0| 98 03 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   187   |1|0|1| 99 03 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   188   |1|0|0| 00 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   189   |1|0|0| 01 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   190   |1|0|1| 02 04 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   191   |1|0|0| 03 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   192   |1|0|0| 76 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   193   |1|0|0| 77 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   194   |1|0|0| 78 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   195   |1|0|0| 79 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   196   |1|0|0| 80 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   197   |1|0|0| 81 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   198   |1|0|0| 82 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   199   |1|0|0| 83 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   200   |1|0|0| 84 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   201   |1|0|0| 85 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   202   |1|0|0| 86 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   203   |1|0|0| 87 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   204   |1|0|1| 88 04 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   205   |1|0|1| 89 04 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   206   |1|0|0| 90 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   207   |1|0|1| 91 04 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   208   |1|0|0| 92 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   209   |1|0|0| 93 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   210   |1|0|1| 94 04 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   211   |1|0|1| 95 04 07 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   212   |1|0|1| 96 04 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   213   |1|0|1| 97 04 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   214   |1|0|0| 98 04 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   215   |1|0|1| 99 04 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   216   |1|0|0| 12 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   217   |1|0|0| 13 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   218   |1|0|0| 14 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   219   |1|0|0| 15 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   220   |1|0|0| 16 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   221   |1|0|0| 17 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   222   |1|0|0| 18 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   223   |1|0|0| 19 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   224   |1|0|0| 20 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   225   |1|0|0| 21 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   226   |1|0|0| 22 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   227   |1|0|0| 23 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   228   |1|0|0| 60 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   229   |1|0|0| 61 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   230   |1|0|0| 62 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   231   |1|0|0| 63 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   232   |1|0|0| 64 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   233   |1|0|0| 65 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   234   |1|0|0| 66 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   235   |1|0|0| 67 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   236   |1|0|0| 68 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   237   |1|0|0| 69 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   238   |1|0|0| 70 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   239   |1|0|0| 71 05 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   240   |1|0|0| 08 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   241   |1|0|0| 09 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   242   |1|0|0| 10 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   243   |1|0|0| 11 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   244   |1|0|0| 12 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   245   |1|0|0| 13 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   246   |1|0|0| 14 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   247   |1|0|0| 15 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   248   |1|0|0| 16 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   249   |1|0|0| 17 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   250   |1|0|0| 18 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   251   |1|0|0| 19 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   252   |1|0|0| 56 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   253   |1|0|0| 57 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   254   |1|0|1| 58 06 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   255   |1|0|0| 59 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   256   |1|0|1| 60 06 07 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   257   |1|0|1| 61 06 07 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   258   |1|0|0| 62 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   259   |1|0|0| 63 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   260   |1|0|0| 64 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   261   |1|0|1| 65 06 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   262   |1|0|0| 66 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   263   |1|0|1| 68 06 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   264   |1|0|0| 69 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   265   |1|0|0| 71 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   266   |1|0|0| 72 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   267   |1|0|1| 73 06 07 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   268   |1|0|0| 74 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   269   |1|0|0| 75 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   270   |1|0|1| 76 06 07 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   271   |1|0|1| 77 06 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   272   |1|0|1| 78 06 07 02 00 00 |  2(B) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   273   |1|0|0| 79 06 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   274   |1|0|1| 16 07 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   275   |1|0|0| 17 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   276   |1|0|1| 18 07 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   277   |1|0|1| 19 07 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   278   |1|0|1| 20 07 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   279   |1|0|0| 21 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   280   |1|0|1| 22 07 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   281   |1|0|1| 23 07 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   282   |1|0|1| 24 07 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   283   |1|0|1| 25 07 07 02 00 00 |  1(A) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   284   |1|0|0| 26 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   285   |1|0|1| 27 07 07 02 00 00 |  3(C) |  X  |  O  | 00/00 | XXXX/0030 |  |
|   286   |1|0|0| 53 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   287   |1|0|0| 54 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   288   |1|0|0| 55 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   289   |1|0|0| 56 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   290   |1|0|0| 57 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   291   |1|0|0| 58 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   292   |1|0|0| 59 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   293   |1|0|0| 60 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   294   |1|0|0| 61 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   295   |1|0|0| 62 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   296   |1|0|0| 63 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   297   |1|0|0| 64 07 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   298   |1|0|0| 16 08 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   299   |1|0|0| 17 08 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   300   |1|0|0| 18 08 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   301   |1|0|0| 19 08 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   302   |1|0|0| 20 08 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   303   |1|0|0| 21 08 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
|   304   |1|0|0| 22 08 07 02 00 00 |   X   |  X  |  X  | 00/00 | XXXX/0030 |  |
----------------------------------------------------------------------------
show_packet_meter_by_alive Total node:         305
----------------------------------------------------------------------------
show_packet_meter_by_alive Active  count:      64
show_packet_meter_by_alive Inactive count:     241
----------------------------------------------------------------------------
show_packet_meter_by_alive Keepalive waiting:  56
show_packet_meter_by_alive Keepalive time min: 30
show_packet_meter_by_alive Keepalive time max: 195
----------------------------------------------------------------------------


vt# check white list from spi 0
check white list from spi 0
already send accept from spi 0
magpie_already_send_accept req->pdc_diff=0, DB.meter_whitelist_table[i].pdc_dif
vtsdk_notify_pan_joined pdc_diff=1
Valid LBD join CB Meter id:64 06 07 02 00 00
EUI64 addr:PHASE: B
64 06 07 ff fe 02 00 00
join_cb >>
64 06 07 02 00 00
join_node meter id is repeat: 64 06 07 02 00 00

```


串台區
```
vtmac_pkt_do_fragmentation() tmr force = 1
Robust retry success(2), Update Neighor table 0x0002(Robust)
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0086)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0087)
[S->D] Bootstrap: Send_eap_psk_msg3
Add Neighbor Table Fail(BlackList 0x0005)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:839606FFFE020000)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry fail
Set Neighbor Table[17] : 0x0002 tmr valid time to zero
notify dcu already send accept fail (1)
notify dcu already send accept fail (2)
notify dcu already send accept fail (3)
notify dcu already send accept fail (4)
notify dcu already send accept fail (5)
notify dcu already send accept fail (6)
cmd delete route
dcu delete route success
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)

Send ToneMap Response(0018), Mod D-0 0xFFFFFF
Robust retry fail
Set Neighbor Table[9] : 0x0018 tmr valid time to zero
cmd delete route
dcu delete route success
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:820207FFFE020000)

vt# LBS: not in whitelist -> decline!!

vt# [D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

vt#
vt# (C)
vt#
vt# [D->S] Bootstrap: Receive_Join


Send ToneMap Response(0001), Mod D-3 0xFF9BFF
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:980407FFFE020000)
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x008e)
[S->D] Bootstrap: Send_eap_psk_msg3
C(0001)Same join again
  30000E4E | 22 08 07 FF FE 02 00 00
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
(TG) 1
msg1 insert tx queue fail
LBS: not in whitelist -> decline!!
(TG) 2
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x008f)
[S->D] Bootstrap: Send_eap_psk_msg3
(TG) 3
msg3 insert tx queue fail
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0090)
[S->D] Bootstrap: Send_eap_psk_msg3
(TG) 4
msg3 insert tx queue fail
Send Beacon (RC_COORD 0x0)
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
vtmac_pkt_do_fragmentation() tmr force = 1
Robust retry fail
Set Neighbor Table[9] : 0x0002 tmr valid time to zero
cmd delete route
dcu delete route success
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
C(0002)[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(0002), Mod D-2 0x00003F
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:629706FFFE020000)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(001B), Mod D-1 0x81905F
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
LBS: not in whitelist -> decline!!
LBS: not in whitelist -> decline!!
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry success(0), Update Neighor table 0x001B(Robust)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Receive beacon_request
    ->form: 171907FFFE020000
LBS: not in whitelist -> decline!!
Add Neighbor Table Fail(BlackList 0x0017)
[D->S] Bootstrap: Recei
Receive beacon_request
    ->form: 438606FFFE020000
ve_Join
[S->D] Bootstrap: Send_eap_psk_msg1

Send ToneMap Response(0017), Mod D-0 0xFFFFFF
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0091)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0092)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Robust retry success(2), Update Neighor table 0x001B(Robust)

Send ToneMap Response(001B), Mod D-1 0x80007F
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:599706FFFE020000)
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:649706FFFE020000)
Add Neighbor Table Fail(BlackList 0x0017)
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0093)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0094)
[S->D] Bootstrap: Send_eap_psk_msg3
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
vtmac_pkt_do_fragmentation() tmr force = 1
Robust retry success(2), Update Neighor table 0x0017(Robust)
Add Neighbor Table Fail(BlackList 0x0017)

Send ToneMap Response(0017), Mod D-0 0xFFFFFF

Send ToneMap Response(0002), Mod D-2 0x00001F
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
LBS: not in whitelist -> decline!!
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0095)
[S->D] Bootstrap: Send_eap_psk_msg3

Send ToneMap Response(001B), Mod D-1 0x80007F
[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:620607FFFE020000)
C(0001)[D->S] Bootstrap: Receive_Join
[S->D] Bootstrap: Send_eap_psk_msg1
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0096)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0097)
[S->D] Bootstrap: Send_eap_psk_msg3
[D->S] Bootstrap: Receive_Msg2
[S->D] Bootstrap: Send_EAP_Result (shortAddr:0x0098)
[S->D] Bootstrap: Send_eap_psk_msg3
Tx Packet Give Up (TxCMP, nack_or_ackto = 3)
Send Beacon (RC_COORD 0x0)

Send ToneMap Response(0002), Mod D-2 0x00003F
Robust retry fail
Set Neighbor Table[9] : 0x0002 tmr valid time to zero
cmd delete route
dcu delete route success
[D->S] Bootstrap: Receive_Msg4
[S->D] Bootstrap: Send_join_accept (LBD:770207FFFE020000)
Add Neighbor Table Fail(BlackList 0x0005)
LBS: not in whitelist -> decline!!

```
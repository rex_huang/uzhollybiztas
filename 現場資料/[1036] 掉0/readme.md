# 集中器 1036

## Richard's note

图片为掉0环境的实际环境图，图123为集中器端环境，图4、5为表端示意图。  
- TXT档6306、stm32 dcu_stm32为9月20日下午录的log。 
- TXT档meter meter2为第一只抄控器改的表的log。 
- TXT档meter3 3.1Luke zhongji为 
- luke请我去找的上一级的log。  
PS: 4.为第一只组网失败的表，抄控器抄表成功，改成表程序和集中器组网失败。 5.为第二只LUKE请我去找的上一级的表。

## 問題
- 日凍結只有70%, 曾經好幾天歸零, 之後又跑到70%

## Noise Floors

- 不通的表的中繼
![screen](第二只Meter的nf/FCC_NoiseFloor_20190921_172547_(Raw).jpg)

- 不通的表
![screen](第一只meter的nf/FCC_NoiseFloor_20190921_161651_(Raw).jpg)


## 環境示意

![screen](掉0环境1.jpg)
![screen](掉0环境2.jpg)
![screen](掉0环境3.jpg)
![screen](掉0环境4.jpg)
![screen](掉0环境5.jpg)



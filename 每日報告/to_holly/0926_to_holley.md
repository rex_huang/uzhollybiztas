# 0926

## 升級準備

- Rex relase出新的image
- 挑升級候選台區: 1.un區 2.數量適中 3.成功率上上下下

    - 1481 164台, 成功率最高80%, 最近掉到40%, 最低有7%
    > 嘗試要了topo跟cq幾次都不行
    - 1313 191台, 最高99%, 最低48%
    - 1536 213台, 最高80%, 最低40%
    - 2861 220台, 10%了幾天, 高到90%, 掉到8%幾天, 又回到70%
    - 2898 142, 底層好通但掉0%

- 用操控寶升級, 已經試過
- 預計明天開始升級

## 掉零現場

### 2898 

這個台區底層通訊沒有太大問題, 拓譜多是1級, 也可以從集中器端跟表通訊

#### 觀察到的問題

1. 路由表的數量比白名單的還要多, 表示有重啟, 如果是因為沒被抄表是符合這個事實的
2. 未重啟前點抄抄得到外掛上去的表, 但是抄不到外掛表pinge能通的表

3. 重啟後掉零狀況消失, 開始補抄日凍結


